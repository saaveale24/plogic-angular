export class Cita {
    codcita!: number;
    placa!: string;
    fecha!: Date;
    fhracrea!: Date;
    codestado!: number;
    estado!: string;
    ftexto!: string;
}
