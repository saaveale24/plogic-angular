import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { catchError, Observable, EMPTY } from 'rxjs';
import { Cita } from './cita';
import { Horario } from './horario';

@Injectable({
  providedIn: 'root'
})
export class CitaService {

  url = 'https://localhost:44362/api/cita';  

  constructor(private http: HttpClient) { }  
  getCita(): Observable<Cita[]> {  
    return this.http.get<Cita[]>(this.url + '/all' ).pipe(
      catchError((err) => {
        return EMPTY;
      })
    ) 
  } 
  getCitaByPlaca(placa: string): Observable<Cita[]> {  
    if(placa==null || placa==''){
      return this.http.get<Cita[]>(this.url + '/all' ).pipe(
        catchError((err) => {
          return EMPTY;
        })
      ) 
    }
    return this.http.get<Cita[]>(this.url + '/' + placa).pipe(
      catchError((err) => {
        return EMPTY;
      })
    )   
  }  
  getHorario(): Observable<Horario[]> {  
    return this.http.get<Horario[]>(this.url + '/horario' ).pipe(
      catchError((err) => {
        return EMPTY;
      })
    ) 
  } 
  createCita(cita: Cita): Observable<Cita> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.post<Cita>(this.url,  
    cita, httpOptions).pipe(
      catchError((err) => {
        return EMPTY;
      })
    )  
  } 
}
