import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';  
import { Observable, isEmpty } from 'rxjs';  
import { CitaService } from '../cita.service';  
import { Cita } from '../cita';
import { Horario } from '../horario';

@Component({
  selector: 'app-cita',
  templateUrl: './cita.component.html',
  styleUrls: ['./cita.component.css']
})
export class CitaComponent implements OnInit {
  dataSaved = false;  
  citaForm: any;  
  allCitas!: Observable<Cita[]>;  
  citaIdUpdate = null;  
  massage = '';  
  listaHoras!: Observable<Horario[]>; 

  

  constructor(private formbulider: FormBuilder, private citaService:CitaService) { }  
  
  ngOnInit() {  
    this.citaForm = this.formbulider.group({  
      placa: ['', [Validators.required]],  
      fecha: ['', [Validators.required]],
      hora: ['', [Validators.required]]
    });  

    this.citaForm.controls['placa'].setValue('');
    this.loadAllCitas();  
    this.loadHorario();
  }  
  loadAllCitas() {  
    this.allCitas = this.citaService.getCita();  
  }  
  buscar() {      
    this.allCitas = this.citaService.getCitaByPlaca(this.citaForm.controls['placa'].value); 
  }  

  loadHorario() {
    this.listaHoras= this.citaService.getHorario(); 
  }

  onFormSubmit() {  
    this.dataSaved = false;  
    const cita = this.citaForm.value; 
    this.CreateCita(cita);  
    this.citaForm.reset();  
  }  
  CreateCita(cita: Cita) {  
    if (this.citaIdUpdate == null) {       
      let f= cita.fecha;

      cita.ftexto=f.getFullYear()+'-'+("00" +(f.getMonth()+1)).slice(-2)+'-'+
      ("00" + f.getDate()).slice(-2)+' '+
      ("00" + new Date(this.citaForm.value.hora).getHours()).slice(-2) + ":" +
      ("00" + new Date(this.citaForm.value.hora).getMinutes()).slice(-2) + ":" +
      ("00" + new Date(this.citaForm.value.hora).getSeconds()).slice(-2);

      this.citaService.createCita(cita).subscribe(  
        () => {  
          this.dataSaved = true;  
          this.massage = 'Registro creado satisfactoriamente';  
          this.loadAllCitas();  
          this.citaIdUpdate = null;  
          this.citaForm.reset();  
        }  
      );  
    }
  }    

  resetForm() {  
    this.citaForm.reset(); 
    this.citaForm.controls['fecha'].setValue(new Date());
    this.massage = '';      
    this.dataSaved = false;  
  }  

}
